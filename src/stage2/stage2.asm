section .stage2
BITS 32
global prepare_stage2
PML4T equ 0x1000
prepare_stage2:
; paging setup at 0x1000 (Level 4)
  mov edi, PML4T
  mov cr3, edi
  xor eax, eax
  mov ecx, 4096
  rep stosd
  mov edi, cr3

  mov dword [edi], 0x2003
  add edi, 0x1000
  mov dword [edi], 0x3003
  add edi, 0x1000
  mov dword [edi], 0x4003
  add edi, 0x1000
  
  mov ebx, 0x3
  mov ecx, 512
  
  .setentry:
  mov dword [edi], ebx
  add ebx, 0x1000
  add edi, 8
  loop .setentry

; enable PAE
  mov eax, cr4
  or eax, 1 << 5
  mov cr4, eax

; set LM-bit of model specfic register (long mode)
  mov ecx, 0xC0000080
  rdmsr
  or eax, 1 << 8
  wrmsr

; enable paging bit (bit 31)
  mov eax, cr0
  or eax, (1 << 31)
  mov cr0, eax


  cli
  mov ax, gdt.data
  mov ds, ax
  mov es, ax
  mov fs, ax
  mov gs, ax
  mov ss, ax

  cli
  lgdt [gdt_ptr]
  extern stage2
  jmp gdt.code:finally64

gdt:
  .null: equ $ - gdt
  dw 0xFFFF                    ; Limit (low).
  dw 0                         ; Base (low).
  db 0                         ; Base (middle)
  db 0                         ; Access.
  db 1                         ; Granularity.
  db 0                         ; Base (high).
  .code: equ $ - gdt
  dw 0                         ; Limit (low).
  dw 0                         ; Base (low).
  db 0                         ; Base (middle)
  db 10011010b                 ; Access (exec/read).
  db 10101111b                 ; Granularity, 64 bits flag, limit19:16.
  db 0                         ; Base (high).
  .data: equ $ - gdt
  dw 0                         ; Limit (low).
  dw 0                         ; Base (low).
  db 0                         ; Base (middle)
  db 10010010b                 ; Access (read/write).
  db 00000000b                 ; Granularity.
  db 0                         ; Base (high).
gdt_ptr:
  dw $ - gdt - 1
  dd gdt

[BITS 64]
finally64:
  extern stage2
  call stage2
  hlt                           ; Halt the processor.
