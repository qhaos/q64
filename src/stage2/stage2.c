#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <stage2/ata.h>
#include <stage2/ext2.h>
#define WIDTH 80
#define HEIGHT 25
#define KERNEL_LOAD (void* )


inode_t* const inode = (inode_t*) 0xBEEF0;
static void* const scratch_buff = (void*)0x80000;
superblock_t* const superblock = (superblock_t*)0x10000;
blockgroup_desctable_t* bgdt = (blockgroup_desctable_t*) (0x10000 + 1024);


uint16_t* vga = (uint16_t*)0xb8000;

static inline void zero(uint8_t* mem,  size_t len) {
  while(len--)
    *(mem++) = 0;
}

static void putchar(uint8_t ch) {
  static size_t idx = 0;
  static uint16_t* const VGA = (uint16_t*) 0xb8000;
  
  if(idx >= WIDTH*HEIGHT) {
    idx = 0;
    zero((uint8_t*)VGA, WIDTH*HEIGHT*2);
  }

  if(ch == '\n') {
    idx /= WIDTH;
    idx++;
    idx *= WIDTH;
  } else {
    VGA[idx++] = 0x0D00 | ch;
  }
}

static void print_u8(uint8_t x) {
  static const char hexstr[] = "0123456789ABCDEF";
  putchar(hexstr[x >> 4]);
  putchar(hexstr[x & 0xF]);
}

static void print_u16(uint16_t x) {
  print_u8(x >> 8);
  print_u8(x >> 0);
}

static void print_u32(uint32_t x) {
  print_u16(x >> 16);
  print_u16(x >> 0);
}

static void read_block(void* dst, uint64_t lba, uint32_t cnt) {
  int status;
  while((status = ata_read(dst, lba << (superblock->block_size+1), cnt << (superblock->block_size+1)))) {
    if(status & (1 | (1 << 5))) {
      ata_reset();
    }
  }
}

static bool streq(char* un, char* dos, size_t cnt) {
  while(cnt--) {
    if(*un != *dos)
      return false;
    un++;
    dos++;
  }
  return true;
}

static void print(char* str, size_t len) {
  while(len--)
    putchar(*(str++));
}

static void ext2_setup() {
  superblock->ext2_signature = 0;
  while(ata_read(superblock, 2, 1))
    ;
  if(superblock->block_size == 0)
    read_block(bgdt, 2, 1);
  else
    read_block(bgdt, 1, 1);
}

void read_inode(void* buff, uint32_t inode) {
  uint32_t bg = (inode-1) / superblock->n_inodes_per_group;

  uint32_t idx = (inode-1) % superblock->n_inodes_per_group;

  uint32_t blk = bgdt[bg].lba_inode_table + ((idx * superblock->sizeof_inode) >> (10+superblock->block_size));
  uint32_t offset = (idx * superblock->sizeof_inode) & ((1 << (10+superblock->block_size)) - 1);

  read_block(buff, blk, 2);

  uint8_t* b = (uint8_t*)(buff + offset);
  uint8_t* d = (uint8_t*)buff;
  for(size_t i = 0; i < superblock->sizeof_inode; i++) {
    d[i] = b[i];
  }
}

static uint32_t find_entry(uint32_t parent_inode, const char* name) {

  uint32_t ret = 0;

  read_inode(inode, parent_inode);
  
  dirent_t* dirent = (dirent_t*) (scratch_buff + 4096);

  size_t n = 0;
  while(inode->dbptr[n]) {
    read_block(dirent, inode->dbptr[n], 1);
    while(dirent->inode) {
      if(streq(dirent->name, name, dirent->namelen)) {
        ret = dirent->inode;
        goto full_break;
      }
      dirent = (dirent_t*)(((void*)dirent) + dirent->sz);
    }

    n++;
  }

  full_break:

  return ret;
}


void stage2() { 
  ata_setup();
  ext2_setup();


  uint32_t sys = find_entry(2, "sys");

  uint32_t stage2bin = find_entry(sys, "hello.txt");

  print_u32(stage2bin);

  read_inode(inode, stage2bin);

  size_t i;
  uint32_t b = 0;
  for(i = 0; inode->dbptr[i]; i++) {
    read_block(scratch_buff + b, inode->dbptr[i], 1);
    b += 1024 << superblock->block_size;
  }

  if(inode->frag_addr) {
    read_block(scratch_buff + b, inode->dbptr[i], superblock->frag_size >> (10 + superblock->block_size));
    b += superblock->frag_size;
  }

  print(scratch_buff, inode->sizelo);

  print_u32(inode->sizelo);

  return;
}


/* ;) */
