; to ignore warnings with resb and zeroing
%macro zerob 1
  times %1 db 0
%endmacro
%macro zerow 1
  zerob 2*%1
%endmacro
%macro zerod 1
  zerow 2*%1
%endmacro
%macro zeroq 1
  zerod 2*%1
%endmacro

[BITS 16]
org 0x7c00

dirent_buff equ 0x1000
STAGE2_LOAD equ 0x5000
MEM_MAP_CNT equ 0x8000
jmp 0x0000:boot
boot:
  xor eax, eax
  mov ds, ax
  mov es, ax
  mov ss, ax
  mov sp, 0x7c00
  mov bp, sp

  mov [disk], dl
  mov byte [step], '1'

; STEP 1, load second half of bootloader, give it 4 tries
  mov di, 1
  mov bx, second_half
  mov al, 1

  call read_sectors

; STEP 2, load superblock
  inc di
  mov bx, superblock
  inc al
  call read_sectors 

  mov si, invalid_ext2_str
  mov ax, 0xef53
  cmp ax, [superblock.signature]
  jne exit

; STEP 3, load bgdt

  xor eax, eax
  test byte [superblock.blocksize], 0xFF
  setz al
  inc al
  mov bx, bgdt
  mov di, 2
  call read_lba

; STEP 4, read inode 2
  mov ax, 2
  call read_inode

; STEP 5, read in the dbptr0 into the dirent buff
  ; the qemu font has 2 as a smiley face, this was here by accident but i'm keeping it in,
  ; if this bootloader makes it this far, it has every right to be happy
  call putchar

  mov ax, [inode.dbptr0]
  mov di, 1
  mov bx, dirent_buff
  call read_lba

; STEP 6, find sys directory, and read it
  mov si, sys_str
  mov cx, sys_str_len
  call find_file
  call read_inode

; STEP 7, repeat 5
  mov ax, [inode.dbptr0]
  mov di, 1
  mov bx, dirent_buff
  call read_lba

; STEP 8, repeat 6, but this time looking to find stage2.in
  mov si, stage2_bin
  mov cx, stage2_bin_len
  call find_file
  call read_inode

; STEPS 9+, load file into memory
step9:
  mov byte [step], 0x30
  mov si, inode.dbptr0
  mov edx, 1024
  mov cx, [superblock.blocksize]
  shl edx, cl
  mov ebx, STAGE2_LOAD
  mov di, 1
  .loop:
  ; get lba
  lodsd
  test eax, eax
  jz .done
  call read_lba
  add ebx, edx
  jmp .loop


  .done:
  mov al, 'h'
  call putchar

  call memmap_load
  call load32


  cli
  hlt


; es:bx is buff ptr, count is in di, lba is in eax
read_lba:
  push cx
  mov cl, [superblock.blocksize]
  inc cl
  shl eax, cl
  shl di, cl
  pop cx
read_sectors:
  pusha
  mov [dap.buff_off], bx
  mov [dap.sectors], di
  mov [dap.lba], eax
  mov al, [step]
  call putchar
  inc byte [step]
  mov dl, [disk]
  mov si, dap
  mov cx, 4

  .loop:
  mov eax, 0x004200
  int 0x13
  jnc .done
  mov si, retry_str
  call puts
  loop .loop
  mov si, failed_str
  jmp exit
  .done:
  mov al, '>'
  call putchar
  popa
  ret


; ch in al
putchar:
  pusha
  mov ah, 0x0e
  cmp al, 10
  jne .next
  .ne_rep:
    int 0x10
    mov al, 13
  .next:
  int 0x10
  popa
  ret

; string in si
puts:
  pusha
  .loop:
  lodsb
  or al, al
  jz .done
  call putchar
  jmp .loop
  .done:
  popa
  ret

; string in si, len in cx
nputs:
  pusha
  .loop:
  lodsb
  call putchar
  loop .loop
  popa
  ret

retry_str:  db 10, 'retrying.....', 0
invalid_ext2_str: db 'missing ext2 magic',0
failed_str: db 'failed.', 0
ok_str: db 'Ok', 10, 0
; error msg in si
exit:
  shr ax, 8
  call print_hex
  call puts
  cli
  hlt


dap:
  .sz:        dw 0x10
  .sectors:   zerob 2
  .buff_off:  zerob 2
  .buff_seg:  zerob 2
  .lba:       zerob 8

zerob 510-($-$$)
dw 0xaa55
second_half:

; eax is inode
read_inode:
  pushad
  dec eax
  xor edx, edx
  div dword [superblock.num_inodes_in_group]
  ; edx is the index now, eax is the group
  shl eax, 5
  add eax, bgdt+8
  mov eax, [eax]
  push eax
  mov eax, edx
  mul dword [superblock.inodesize]
  mov ebx, eax
  mov cl, [superblock.blocksize]
  add cl, 10
  shr eax, cl
  pop edx

  add eax, edx
  mov edx, ebx
  mov bx, inode
  mov di, 1
  call read_lba

  mov eax, 1
  shl eax, cl
  dec eax
  and eax, edx
  mov ecx, eax
  mov esi, eax
  add esi, inode
  mov edi, inode
  rep movsb

  popad
  ret

print_hex:
  pusha
  mov dx, ax
  mov ah, 0x0E
  mov cx, 4
  .loop:
    mov al, dh
    shr al, 4
    cmp al, 10
    jl .hex_digit
    ; if it's not 0-9, we can't just add 0x30
    add al, ('A'-'0'-10)
    .hex_digit:
    add al, '0'
    shl dx, 4
    int 0x10
  loop .loop
  mov al, ' '
  call putchar
  popa
  ret


; name of entry in si
; inode returns in ax, exit if not found
find_file:
; dirents : {
;   u32 inode;
;   u16 dirent_size;
;   u8 name_len
;   u8 type_indicator/hi_name_len(ignored)
;   u8 name[]
; }
  mov bx, dirent_buff
  .loop:
    mov ax, [bx]
    test ax, ax
    jz .fail
    lea di, [bx + 8]
    call strncmp
    je .done
    .continue:
    add bx, [bx + 4]
    jmp .loop
  .fail:
  mov si, file_not_found
  jmp exit
  .done:
  ret

strncmp:
  pusha
  .loop:
    cmpsb
    jne .exit
  loop .loop
  .exit:
  popa
  ret


memmap_load:
  push ebp
  ; detect memory and put it in a known location
  mem_mappit:
  mov di, MEM_MAP_CNT + 0x4
  xor ebx, ebx
  xor bp, bp
  mov edx, 0x0534D4150        ; signature
  mov eax, 0xe820             ; function code
  mov dword [es:di + 20], 1   ; forces ACPI 3.X entry
  mov ecx, 24                 ; 24 byte request (u64 base, u64 len, u32 type, u32 acpi)
  int 0x15
  mov edx, 0x0534D4150
  jmp short .memloop

  .repeat:
  mov eax, 0xe820
  mov dword [es:di + 20], 1
  mov ecx, 24
  int 0x15
  jc short .end               ; carry means done!
  mov edx, 0x0534D4150

  .memloop:
  jcxz .skip
  cmp cl, 20                  ; check if the response was 20 or 24 bytes
  jbe .nodice
  test byte [es:di + 20], 1   ; if bit 0 of ACPI is set, ignore it
  je short .skip

  .nodice:
  mov ecx, [es:di + 8]        ; get low u32 of length
  or ecx, [es:di + 12]        ; or it with the high to see if it is len = 0
  jz .skip
  inc bp
  add di, 24

  .skip:
  test ebx, ebx               ; if zero, then we are done
  jnz .repeat

  .end:
  mov [MEM_MAP_CNT], bp

  pop ebp
  ret


load32:  ; switch to mode Text Mode 03h
  mov ax, 0x0003
  int 0x10
  ; ======= get to 32-bit mode =======
  ; 1. trip A20
  ; 2. Load GDT, flat is fine for now

  mov ax, 0x2401
  int 0x15

  cli
  lgdt [gdt_ptr]
  mov eax, cr0
  or eax, 0x01
  mov cr0, eax

  jmp 8:lbl

  lbl:
BITS 32
  mov ebx, 0xb8000
  mov word [ebx], 0x1f30
  jmp STAGE2_LOAD
  hlt

gdt_ptr:
  dw 24
  dd gdt

gdt:
  .null:
    dq 0
  .code:
  dw 0xFFFF
  dw 0x0000
  db 0x0000
  db 0b10011110
  db 0b1100_1111
  db 0x00
  .dat:
  dw 0xFFFF
  dw 0x0000
  db 0x00
  db 0b10010110
  db 0b1100_1111
  db 0x00



file_not_found: db 'boot file not found!', 0
sys_str: db 'sys'
sys_str_len equ 3
stage2_bin: db 'stage2.bin'
stage2_bin_len equ 10
zerob 1024-($-$$)
disk: zerob 1
step: zerob 1
%include "include/boot/asm/superblock.asm"
zerob 1024
%include "include/boot/asm/blockgrouptable.asm"
bgdt:
zerob 4096
%include "include/boot/asm/inode.asm"
