inode:
; permissions
  .perms:         zerob 2
; User ID
  .uid:           zerob 2
; low 32-bits of size
  .size_lo:       zerob 4
; POSIX time of last access
  .last_access:   zerob 4
; POSIX time of creation
  .creation_time: zerob 4
; POSIX time of last modification
  .last_mod:      zerob 4
; POSIX time of deletion
  .deletion_time: zerob 4
; Groupd ID
  .guid:          zerob 2
; Count of hard links to this, (when 0, makred as unalloc'd)
  .num_hardlinks: zerob 2
; Count of DISK SECTORS in use (not counting inode structure or directory entries)
  .num_disksects: zerob 4
; Flags
  .flags:         zerob 4
; Operating System Specific Value 1, ignore
  .OSV1:          zerob 4
; Direct Block Pointers
  .directblockptrs:
    .dbptr0:    zerob 4
    .dbptr1:    zerob 4
    .dbptr2:    zerob 4
    .dbptr3:    zerob 4
    .dbptr4:    zerob 4
    .dbptr5:    zerob 4
    .dbptr6:    zerob 4
    .dbptr7:    zerob 4
    .dbptr8:    zerob 4
    .dbptr9:    zerob 4
    .dbptr10:   zerob 4
    .dbptr11:   zerob 4
; Singly indirect block pointer
  .sibptr:       zerob 4
; Doubly indirect block pointer!!!
  .dibptr:       zerob 4
; TRIPLY!!! indirect block pointer!!!!!!
  .tibptr:       zerob 4
; Generation number
  .gen_num:       zerob 4
; Extended attribute block
  .eab:           zerob 4
; If it's a file, upper 32-bits of size (if possible, check feature bits), if Directory, ACL
  .size_hi:
  .directory_acl: zerob 4
; Block address of fragment
  .frag_block:    zerob 4
; OSV 2
  .OSV2:
    .frag_num:    zerob 1
    .frag_size:   zerob 1
  zerob 10