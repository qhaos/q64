superblock:
; Total number of inodes in the FS
  .num_total_inodes:      zerob 4
; Total number of blocks in the FS
  .num_total_blocks:      zerob 4
; Number of blocks resevered for superuser
  .num_resv_for_super:    zerob 4
; Number of unallocated blocks
  .num_unalloc_blocks:    zerob 4
; Number of unallocated inodes
  .num_unalloc_inodes:    zerob 4
; Block number of superblock
  .superblock_number:     zerob 4
; (1024 << blocksize) = bytes
  .blocksize:             zerob 4
; (1024 << fragsize) = bytes
  .fragsize:              zerob 4
; Number of blocks in each block group
  .num_blocks_in_group:   zerob 4
; Number of fragments in each block group
  .num_frags_in_group:    zerob 4
; Number of inodes in each block group
  .num_inodes_in_group:   zerob 4
; Last mount time
  .last_mount_time:       zerob 4
; Last written time
  .last_written_time:     zerob 4
; Number of mounts since last fsck
  .last_mnts_since_fsck:  zerob 2
; Number of mounts allowed 'twix fscks
  .fsk_mnt_interval:      zerob 2
; Ext2 signature (0xef53)
  .signature:             zerob 2
; File system health (1 = clean, 2 = errorful)
  .health:                zerob 2
; Error severity (1 = ignore, 2 = remount as read-only, 3 = panic!)
  .error_handling:        zerob 2
; Version minor
  .version_minor:         zerob 2
; Time of last fsck
  .time_of_last_fsck:     zerob 4
; Time interval betwix fsck
  .fsck_time_interval:    zerob 4
; Operating system ID
  .vendor_id:             zerob 4
; Version major
  .version_major:         zerob 4
; UID that can use reserved blocks
  .uid_for_reserved:      zerob 2
; GUID that can use reserved blocks
  .guid_for_reserved:     zerob 2
; First non-reserved inode
  .first_non_reserved:    zerob 4
; Size of inode structure
  .inodesize:          zerob 4