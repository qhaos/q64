CC :=clang --target=x86_64-pc-none-elf
CFLAGS := -Iinclude -mcmodel=large -mno-red-zone -mno-mmx -mno-sse -mno-sse2 -ffreestanding -fno-builtin -nostdlib -Os -Wall
BOOTDIR := src/boot
STAGE2DIR := src/stage2
OBJDIR := obj
STAGE2OUT := mnt/sys/stage2.bin

all: stage2 boot fs


stage2: $(STAGE2DIR)/stage2.asm $(STAGE2DIR)/stage2.c
	mkdir -p $(OBJDIR)/stage2
	nasm -felf64 src/stage2/stage2.asm -o $(OBJDIR)/stage2/stage2load.o
	$(CC) $(CFLAGS) -c $(STAGE2DIR)/stage2.c -o $(OBJDIR)/stage2/stage2.o
	$(CC) $(CFLAGS) -c $(STAGE2DIR)/util/*.c
	mv *.o $(OBJDIR)/stage2
	$(CC) $(CFLAGS) -T build/stage2.ld -o $(STAGE2OUT) $(OBJDIR)/stage2/*.o -lgcc

boot: $(BOOTDIR)/boot.asm
	nasm $(BOOTDIR)/boot.asm -o $(OBJDIR)/boot.bin

fs:
	fortune -lo > mnt/hello.txt
	fortune -la >> mnt/hello.txt
	fortune -l 	>> mnt/hello.txt
	dd if=/dev/zero of=disk.img bs=1024 count=4096
	mkfs.ext2 disk.img -d mnt
	dd if=$(OBJDIR)/boot.bin of=disk.img bs=1024 count=1 conv=notrunc

run: disk.img
	wc -c $(STAGE2OUT)
	qemu-system-x86_64 disk.img

clean:
	rm -r $(OBJDIR)/*
